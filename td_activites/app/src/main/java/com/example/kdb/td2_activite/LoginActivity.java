package com.example.kdb.td2_activite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
    }
    public EditText username;
    public EditText pwd;

    public void login(View v){

        EditText username = (EditText) findViewById(R.id.username);
        EditText pwd = (EditText) findViewById(R.id.pwd);

        String StringUser = username.getText().toString();
        String StringPwd = pwd.getText().toString();

        if(StringPwd.equals("admin") && StringUser.equals("admin")){

            Intent intent = new Intent(LoginActivity.this, NewsActivity.class);
            intent.putExtra("Login","admin");
            startActivity(intent);

        }else{
            Toast toast = Toast.makeText( getApplicationContext(), "Username or Password incorrect, Try again !", Toast.LENGTH_LONG);
            toast.show();
        }

    }

}
