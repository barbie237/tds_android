package com.example.kdb.td2_activite;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class NewsActivity extends AppCompatActivity{

    String Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            Login = bundle.getString("Login");

        }
        TextView rslt = (TextView) findViewById(R.id.rslt);
        rslt.setText(Login);

    }

    public void details(View v){
        Intent intent = new Intent(NewsActivity.this, DetailsActivity.class);
        intent.putExtra("Login",Login);
        startActivity(intent);
    }

    public void logout(View v){
        Intent intent = new Intent(NewsActivity.this, LoginActivity.class);
        startActivity(intent);
    }


    public void OnBackPressed(){

    }
}
