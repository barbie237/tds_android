package com.example.kdb.td2_activite;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        String Login;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            Login = bundle.getString("Login");
        }
    }

    public void ok(View v){
        Intent intent = new Intent(DetailsActivity.this, NewsActivity.class);
        startActivity(intent);
    }
}
