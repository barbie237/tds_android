package com.example.kdb.td1_calculatrice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public EditText chiffre1;
    public EditText chiffre2;
    public TextView resultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Ajouter(View v){
        EditText numero1 =(EditText) findViewById(R.id.chiffre1);
        EditText numero2 =(EditText) findViewById(R.id.chiffre2);

        int num = Integer.parseInt(numero1.getText().toString());
        int chiffre2 = Integer.parseInt(numero2.getText().toString());
        int resultat = num + chiffre2;
        TextView total = (TextView )findViewById(R.id.resultat);
        total.setText(Integer.toString(resultat));

    }


    public void soustra(View v){
        EditText numero1 =(EditText) findViewById(R.id.chiffre1);
        EditText numero2 =(EditText) findViewById(R.id.chiffre2);

        int num = Integer.parseInt(numero1.getText().toString());
        int chiffre2 = Integer.parseInt(numero2.getText().toString());
        int resultat = num - chiffre2;
        TextView total = (TextView )findViewById(R.id.resultat);
        total.setText(Integer.toString(resultat));

    }

    public void division(View v){
        EditText numero1 =(EditText) findViewById(R.id.chiffre1);
        EditText numero2 =(EditText) findViewById(R.id.chiffre2);

        int num = Integer.parseInt(numero1.getText().toString());
        int chiffre2 = Integer.parseInt(numero2.getText().toString());

        if (chiffre2 == 0){

            Toast toast = Toast.makeText( getApplicationContext(), "Impossible", Toast.LENGTH_LONG);
            toast.show();
        }else {
            int resultat = num / chiffre2;
            TextView total = (TextView) findViewById(R.id.resultat);
            total.setText(Integer.toString(resultat));
        }
    }

    public void multi(View v){
        EditText numero1 =(EditText) findViewById(R.id.chiffre1);
        EditText numero2 =(EditText) findViewById(R.id.chiffre2);

        int num = Integer.parseInt(numero1.getText().toString());
        int chiffre2 = Integer.parseInt(numero2.getText().toString());
        int resultat = num * chiffre2;
        TextView total = (TextView )findViewById(R.id.resultat);
        total.setText(Integer.toString(resultat));
    }
}
