package com.example.td3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//CREER ICI UNE LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLISSEZLA DE JeuxVideo
        List<JeuVideo> mesJeux =  new ArrayList<>();
        JeuVideo jeuVideo1 = new JeuVideo("PS4", 1000);
		mesJeux.add(jeuVideo1);
		
		JeuVideo jeuVideo2 = new JeuVideo("WII", 600);
		mesJeux.add(jeuVideo2);
		
		JeuVideo jeuVideo3 = new JeuVideo("INTENDO", 800);
		mesJeux.add(jeuVideo3);

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter(new MyVideosGamesAdapter(mesJeux));

    }
}
